Wiser Count

### Steps to setup project

- Clone the repository

```bash
git clone https://gitlab.com/nikhil25803/wisercount-landing-page.git
```

- Change directory

```bash
cd wisercount-landing-page
```

- Install Dependencies

```bash
npm install
```

- Start the server

```bash
npm run dev
```
