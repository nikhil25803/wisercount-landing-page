/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    colors: {
      colorBlue: "#090E82",
      colorLight: "#ECEDFF",
      colorWhite: "#FFFFFF",
      textDark: "#121212",
      textGrey: "#323232",
      textInput: "#6A6A6A",
    },
  },
  plugins: [],
};
