import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Homepage from "./pages/Homepage";
import EventPage from "./pages/EventPage";
function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/event" element={<EventPage />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
