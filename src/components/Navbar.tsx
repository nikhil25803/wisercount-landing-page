import { useState } from "react";
import WiserCountLogo from "../assets/logo.png";
import { CgProfile } from "react-icons/cg";

const Navbar = () => {
  const [menu, setMenu] = useState(false);

  const toggleOptions = () => {
    setMenu(!menu);
  };

  return (
    <nav>
      <div className="flex flex-row items-center justify-between">
        <div className="flex-row items-center justify-center text-xl font-semibold">
          <div className={menu ? "hidden" : "flex flex-row items-center"}>
            <img src={WiserCountLogo} alt="Wiser Count Logo" />
            <h1>WiserCount</h1>
          </div>
        </div>
        {menu ? (
          <>
            <div className="flex flex-col w-full mb-5 gap-y-2 md:hidden">
              <div className=" bg-colorLight w-3/4 rounded-full">
                <p className="p-3 text-colorBlue font-semibold">
                  RecentEventId
                </p>
              </div>
              <div className="flex flex-row items-center justify-start gap-x-2">
                <div className="text-3xl">
                  <CgProfile />
                </div>
                <div>
                  <h1 className="font-semibold">Test User</h1>
                  <p>abc@work.com</p>
                </div>
              </div>
            </div>
          </>
        ) : (
          <></>
        )}
        <div className="hidden md:block bg-colorLight w-1/2 rounded-full">
          <p className="p-3 text-colorBlue font-semibold">RecentEventId</p>
        </div>
        <div className="hidden md:flex flex-row items-center justify-center gap-x-2">
          <div className="text-4xl">
            <CgProfile />
          </div>
          <div>
            <h1 className="font-semibold">Test User</h1>
            <p>abc@work.com</p>
          </div>
        </div>
        <div className="md:hidden flex flex-row items-center justify-start gap-x-2 ">
          <button onClick={toggleOptions} className="text-xl font-semibold">
            {menu ? <>X</> : <>Menu</>}
          </button>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
