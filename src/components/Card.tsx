interface CameraCardInfo {
  cameraName: string | null;
  cameraLocation: string;
  cameraImage: string;
}

const Card = (props: CameraCardInfo) => {
  return (
    <div className="justify-center">
      <img
        src={props.cameraImage}
        alt="card"
        width={181}
        height={250}
        className="w-full"
      />
      <div className="flex flex-col items-center">
        <h1>{props.cameraName}</h1>
        <p>{props.cameraLocation}</p>
      </div>
    </div>
  );
};

export default Card;
