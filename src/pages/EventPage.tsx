import { IoSettingsOutline } from "react-icons/io5";
import Card from "../components/Card";
import { CAMERADATA } from "../data/camera-data";
import Navbar from "../components/Navbar";

const EventPage = () => {
  return (
    <section className="w-full">
      <div className="py-2 px-10">
        <Navbar />
        <div className="my-10 border border-textInput rounded-lg p-5 flex flex-col gap-y-5 md:flex-row justify-between items-center">
          <div className="grid grid-cols-1 sm:grid-cols-2 gap-y-5 gap-x-10">
            <div className="flex flex-col ">
              <p className="font-semibold text-base">Wed, 25 Jan 2024</p>
              <p className="font-semibold text-2xl">Saurabh's Birthday Party</p>
              <p className="text-base font-semibold">
                HappyDreams Hotel Hall 5 New Delhi
              </p>
            </div>
            <div className="self-end text-base">
              <p className="font-semibold">250 attendees</p>
              <p>Min. Commitment</p>
            </div>
          </div>
          <div className="">
            <button className="bg-colorBlue text-colorWhite px-10 py-2 rounded-full">
              Start Event
            </button>
          </div>
        </div>

        <div className="flex flex-row justify-between items-center ">
          <div className="flex flex-col">
            <h1 className="font-semibold text-2xl">Camera Setup</h1>
            <p>Finalise current event's camera setup & get started</p>
          </div>
          <IoSettingsOutline className="text-2xl" />
        </div>
        <div className="mt-5 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-6  gap-5">
          {CAMERADATA.map((cdata, index) => {
            return (
              <Card
                key={index}
                cameraImage={cdata.cameraImage}
                cameraName={cdata.cameraName}
                cameraLocation={cdata.cameraLocation}
              />
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default EventPage;
