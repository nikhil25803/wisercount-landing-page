import Background from "../assets/background.png";
import WiserCountLogo from "../assets/logo.png";

const Homepage = () => {
  return (
    <section className="w-full font-sans">
      <div className="grid grid-cols-3">
        <div className="hidden lg:block  lg:col-span-2">
          <img src={Background} alt="banner" className="h-screen" />
        </div>

        <div className="col-span-3 lg:col-span-1 self-center p-20">
          <div className="flex flow-row items-center justify-center text-2xl text-colorBlue font-semibold">
            <img src={WiserCountLogo} alt="Wiser Count Logo" />
            <h1>WiserCount</h1>
          </div>
          <div className="flex flex-col items-center mt-16 gap-y-2">
            <h1 className="text-3xl font-semibold">Welcome Back</h1>
            <p className="text-textGrey">Enter your details</p>
          </div>
          <div>
            <form className="flex flex-col">
              <label htmlFor="input-email" className="mt-5 font-semibold">
                Email
              </label>
              <input
                placeholder="abc@work.com"
                type="email"
                id="input-email"
                className="p-2 border border-textInput rounded-full"
              />
              <label htmlFor="input-email" className="mt-5 font-semibold">
                Password
              </label>
              <input
                placeholder="Enter Password"
                type="password"
                id="input-password"
                className="p-2 border border-textInput rounded-full"
              />
              <p className="mt-2 text-sm self-end">Forgot Password?</p>
              <button className="mt-5 bg-colorBlue text-colorWhite p-4 rounded-full">
                Sign In
              </button>
            </form>
          </div>
          <div className="text-sm flex justify-center my-10">
            Don't have an account?{" "}
            <a href="#" className="text-colorBlue">
              Sign Up
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Homepage;
